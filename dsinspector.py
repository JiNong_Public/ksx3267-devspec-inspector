#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 JiNong, Inc.
# All right reserved.
#

import sys
import json

KEYWORDS_MO = {
    "mandatory" : ["Class", "Type", "CommSpec"],
    "optional" : ["Model", "Name", "Vendor-Specific", "Channel"],
    "sensor" : {"mandatory" : ["ValueUnit", "ValueType"], "optional":["SignificantDigit"]},
    "node" : {"mandatory" : [], "optional":["Devices"]}
}

KEYWORDS_TYPES = {
    "Class" : {"type": "Class"},
    "Type" : {"type": "DevType"},
    "Model" : {"type": "string"},
    "Name" : {"type": "string"},
    "ValueUnit" : {"type": "ValueUnit"},
    "ValueType" : {"type": "ValueType"},
    "Channel" : {"type": "int16"},
    "SignificantDigit" : {"type": "int16"},
    "CommSpec" : {"type": "CommSpec"},
    "Devices" : {"type": "Devices"},
    "Vendor-Specific" : {"type": "Object"}
}

VALUE_TYPES = ["float", "int"]
VALUE_UNITS = range(1, 14) + [
    "℃", "celsius", 
    "%", "percentage",
    "µmol/mol", "ppm",
    "W/㎡", 
    "°", "degree",
    "m/s",
    "", "no-unit",
    "µmol/㎡/s",
    "%vol.",
    "kPa",
    "dS/m",
    "pH",
    "℉", "fahrenheit"
]

DEV_TYPES = {
    "node" : [
        "sensor-node",
        "actuator-node",
        "integrated-node",
        "nutrient-supply-node"
    ],
    "sensor": [
        "temperature-sensor",
        "humidity-sensor",
        "CO₂-sensor",
        "pyranometer",
        "wind-direction-sensor",
        "wind-speed-sensor",
        "rain-detector",
        "quantum-sensor",
        "soil-moisture-sensor",
        "tensiometer",
        "electrical-conductivity-sensor",
        "hydrogen-exponent-sensor",
        "soil-temperature-sensor"
    ],
    "actuator": [
        "switch/level0",
        "switch/level1",
        "switch/level2",
        "retractable/level0",
        "retractable/level1",
        "retractable/level2",
        "nutrient-supply/level0",
        "nutrient-supply/level1"
    ]
}

COMM_SPEC = {
    "KS-X-3267:2018" : {
        "read": {
            "starting-register" : {"mo" : "m", "type": "int16"},
            "items" : { 
                "node": {
                    "mandatory" : ["status"],
                    "optional" : None
                },
                "sensor": {
                    "mandatory" : ["status", "value"],
                    "optional" : None
                },
                "switch/level0": {
                    "mandatory" : ["status", "opid"],
                    "optional" : ["state-hold-time"]
                },
                "switch/level1": {
                    "mandatory" : ["status", "remain-time", "opid"],
                    "optional" : ["state-hold-time"]
                },
                "switch/level2": {
                    "mandatory" : ["status", "ratio", "remain-time", "opid"],
                    "optional" : ["state-hold-time"]
                },
                "retractable/level0": {
                    "mandatory" : ["status", "opid"],
                    "optional" : ["state-hold-time"]
                },
                "retractable/level1": {
                    "mandatory" : ["status", "remain-time", "opid"],
                    "optional" : ["position", "state-hold-time"]
                },
                "retractable/level2": {
                    "mandatory" : ["status", "remain-time", "position", "opid"],
                    "optional" : ["state-hold-time"]
                },
                "nutrient-supply/level0": {
                    "mandatory" : ["control", "status", "area", "alert", "opid"],
                    "optional" : None
                },
                "nutrient-supply/level1": {
                    "mandatory" : ["control", "status", "area", "alert", "opid"],
                    "optional" : None
                }
            }
        },
        "write": {
            "starting-register" : {"mo" : "m", "type": "int16"},
            "items" : {
                "switch/level0": {
                    "mandatory" : ["operation", "opid"],
                    "optional" : None
                },
                "switch/level1": {
                    "mandatory" : ["operation", "opid", "hold-time"],
                    "optional" : None
                },
                "switch/level2": {
                    "mandatory" : ["operation", "opid", "hold-time", "ratio"],
                    "optional" : None
                },
                "retractable/level0": {
                    "mandatory" : ["operation", "opid"],
                    "optional" : None
                },
                "retractable/level1": {
                    "mandatory" : ["operation", "opid", "time"],
                    "optional" : ["opentime", "closetime"]
                },
                "retractable/level2": {
                    "mandatory" : ["operation", "opid", "position"],
                    "optional" : ["opentime", "closetime"]
                },
                "nutrient-supply/level0": {
                    "mandatory" : ["operation", "opid", "control"],
                    "optional" : None
                },
                "nutrient-supply/level1": {
                    "mandatory" : ["operation", "opid", "control", "EC", "pH", "on-sec", "start-area", "stop-area"],
                    "optional" : None
                }
            }
        }
    }
}

class DevSpecInspector:
    def loadspec(self, fname):
        fp = open(fname, "r")
        spec = json.loads(fp.read())
        fp.close()
        return spec

# def checkmo(self, spec, key, mo):
#        if mo[0] == "s": assert spec["Class"] == "sensor", "Only sensor can has this keyword " + str(key)
#        if mo[0] == "n": assert spec["Class"] == "node", "Only node can has this keyword " + str(key)

    def comparelist(self, val, lst, msg):
        for v in lst:
            if val.encode('utf-8') == v:
                return True
        assert False, msg

    def checktype(self, spec, key, tp):
        if tp == "DevType": 
            self.comparelist(spec[key], DEV_TYPES[spec["Class"]], "Type[" + spec[key].encode('utf-8') + "] should be in " + str(DEV_TYPES[spec["Class"]]))
        if tp == "ValueUnit": 
            self.comparelist(spec[key], VALUE_UNITS, "ValueUnit[" + spec[key].encode('utf-8') + "] should be a proper unit.")
        if tp == "ValueType": 
            self.comparelist(spec[key], VALUE_TYPES, "ValueType[" + spec[key].encode('utf-8') + "] should be in " + str(VALUE_TYPES))
        if tp == "int16" : 
            assert type(spec[key]) is int, str(key) + "should be an integer"
        if tp == "CommSpec": 
            self.checkcommspec(spec, key)
        if tp == "Devices": 
            for dev in spec["Devices"]:
                self.inspect(dev)

    def checkcommspec(self, spec, key):
        for proto in spec[key].keys():
            assert proto in COMM_SPEC.keys(), "There is no matched protocol " + str(proto)
            if spec["Class"] in ("actuator", "nutrient-supply") :
                tp = spec["Type"]
            else:
                tp = spec["Class"]

            for rw, v in spec[key][proto].iteritems():
                assert rw in COMM_SPEC[proto].keys()
                assert type(spec[key][proto][rw]["starting-register"]) is int, "CommSpec[" + str(rw) + "] starting-register should be an integer(address)"
                assert self.checkmo(spec[key][proto][rw]["items"], COMM_SPEC[proto][rw]["items"][tp]["mandatory"], COMM_SPEC[proto][rw]["items"][tp]["optional"]), "CommSpec[" + str(rw) + "] items is not matched."

    def checkmo(self, item, mandatory, optional):
        m = set(mandatory)
        i = set(item)
        assert m == (m & i), "Mandatory keyword(" + str(m) + "," + str(i) + ") is missing. "
        if optional:
            o = set(optional)
            assert o.issuperset(i.difference(m)), "Optional keyword is not matched." + str(i.difference(m).difference(o))
        return True

    def checkkeyword(self, spec):
        assert "Class" in spec, "There is no Class keyword."
        assert spec["Class"] in DEV_TYPES.keys(), "Class should be in " + str(DEV_TYPES.keys())

        if spec["Class"] in ("node", "sensor"):
            self.checkmo(spec.keys(), KEYWORDS_MO["mandatory"] + KEYWORDS_MO[spec["Class"]]["mandatory"], 
                    KEYWORDS_MO["optional"] + KEYWORDS_MO[spec["Class"]]["optional"])
        else:
            self.checkmo(spec.keys(), KEYWORDS_MO["mandatory"], KEYWORDS_MO["optional"])

    def inspect(self, spec):
        self.checkkeyword(spec)

        for k in spec.keys():
            self.checktype(spec, k, KEYWORDS_TYPES[k]["type"])
        return True

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage : python dsinspector.py specfile"
        sys.exit(2)
    
    dsi = DevSpecInspector()
    spec = dsi.loadspec(sys.argv[1])
    dsi.inspect(spec)
